package com.learnig.ventast.controller;

import com.learnig.ventast.model.Usuario;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@RestController
public class AccesoController {

    @PostMapping(value = "acceso", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Usuario> login(@RequestParam("usuario") final String usuario, @RequestParam("password") final String password) {

        String token = getJWTToken(usuario);
        Usuario usuario1 = new Usuario();
        usuario1.setIdUsuario(usuario);
        usuario1.setToken("Ok");
        return ResponseEntity.ok().header("token", token).body(usuario1);

    }

    private String getJWTToken(final String username) {
        String secretKey = "secretKey";
        List<GrantedAuthority> grantedAuthorities = AuthorityUtils
                .commaSeparatedStringToAuthorityList("ROLE_CLIENTE");

        String token = Jwts
                .builder()
                .setId("truperJWT")
                .setSubject(username)
                .claim("authorities",
                        grantedAuthorities.stream()
                                .map(GrantedAuthority::getAuthority)
                                .collect(Collectors.toList()))
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 600000))  // tiempo de vida
                .signWith(SignatureAlgorithm.HS512,
                        secretKey.getBytes()).compact();

        return token;
    }
}
