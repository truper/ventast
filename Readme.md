# Documentación

## 1. Ejemplo Prueba Técnica Ventas  

En esta documento se describe los pasos para la creación de un servicio con una base de datos H2 bajo los siguientes requerimientos:

1. Creación de modelo **Entidad-Relación** de base de datos en H2 Database (En memoria).
2. Creación del servicio **REST** que exponga el modelo CRUD.
3. Agregar seguridad al servicio REST con **JWT**.
4. Por medio de **AOP** agregar un log que mida el tiempo de ejecución.
5. Consumir el servicio REST desde un Front (AngularJS o ReasctJS).

## 2. Base de datos H2
Para ingresar a la base de datos una vez iniciado el servicio, abrir en el navegador la url
[http://localhost:8080/h2-console](http://localhost:8080/h2-console)
 e introducir los siguientes datos:
* Driver Class: **org.h2.Driver**
* JDBC URL: **jdbc:h2:mem:truperdb**
* User Name: **dba1**
* Password(Sin contraseña): 

Estos datos los configuras en el archivo properties (application.properties/yaml) de la aplicación.

## 3. Desarrollo del API

#### 3.1 Modelado de la base de datos
Para la estructura de paquetes, se dejo toda la parte de base de datos dentro del paquete modelo
* Para evitar la recursión infinita en una relación ```@OneToMany``` ```@ManyToOne```   es necesario usar las anotaciones ```@JsonManagedReference``` y ```@JsonBackReference```, 
puedes ver más detalle de su uso en el siguiente enlace [https://www.keenformatics.com/how-to-solve-json-infinite-recursion/](https://www.keenformatics.com/how-to-solve-json-infinite-recursion/).
* Para generar una tabla relacional con una llave primaria compuesta se puede usar la anotación ```@IdClass(ListaDetalleId.class)```, puedes ver más detalle de su uso en el siguiente enlace [https://www.oscarblancarteblog.com/2016/11/02/llaves-compuestas-idclass/](https://www.oscarblancarteblog.com/2016/11/02/llaves-compuestas-idclass/).
* La interacción con la base de datos se puede hacer desde la interface ClienteRepository con el guardado en cascada.

## 4. Ejemplo de Requests y Responses 
Se adjunta la Collection de Postman para una mayor comprensión [TruperTest.postman_collection.json](https://gitlab.com/truper/ventast/-/blob/develop/src/main/resources/TruperTest.postman_collection.json?ref_type=heads).

### 4.1 Generar Token(JWT)
Se genera el token(JWT) para la interacción con los end-point's.
* **URL:** [HOST]:[PORT]/acceso
* **Content-Type:** application/x-www-form-urlencoded
* **HTTP Method:** POST
* **usuario:** "usuario"
* **password:** "secretKey"

## 3.1. Requests y Responses Consulta clientes 
* **URL:** <HOST>:<PORT>/api/clientes
* **Content-Type:** application/json
* **HTTP Method:** GET

### 3.2. Response Clientes
**Ejemplo de Response:**
```json
[
 {
  "idCliente": 720010,
  "listaCompras": [
   {
    "idListaCompra": 1,
    "nombre": "Lista Mensual",
    "listaDetalles": [
     {
      "idListaCompra": 1,
      "idProducto": 18157,
      "cantidad": 5
     }
    ]
   }
  ]
 }
]
```

### 4. Request Guardar Lista compras
* **URL:** <HOST>:<PORT>/api/listaCompras/agregar/cliente
* **Content-Type:** application/json
* **HTTP Method:** POST
**Ejemplo de Request:**
```json
{
    "idCliente": 720010,
    "listaCompras": [
        {
            "nombre": "Lista Mensual",
            "listaDetalles": [
                {
                    "idProducto": 18156,
                    "cantidad": 5
                }
            ]
        }
    ]
}
```

### 4.1. Response Clientes
**Ejemplo de Response:**
```json
{
 "idCliente": 720010,
 "listaCompras": [
  {
   "idListaCompra": 2,
   "nombre": "Lista Mensual",
   "listaDetalles": [
    {
     "idListaCompra": 2,
     "idProducto": 18156,
     "cantidad": 5
    }
   ]
  }
 ]
}
```

## 3. Tecnologías

Para funcionar correctamente son necesarias:

* **[H2 Database]** - H2 es un sistema administrador de bases de datos relacionales programado en Java.
* **[Spring Boot]** - Spring Boot es la solución de convención sobre configuración de Spring para la creación de Microservicios, en un esquema productivo.
* **[Spring JPA]** - 
* **[Spring Web]** - 
* **[Gradle]** - Es un sistema de automatización de construcción de código de software que construye sobre los conceptos de Apache Ant y Apache Maven e introduce un lenguaje específico del dominio (DSL).

## 4. Instalación

Standalone requiere [JDK 8](http://www.oracle.com).

**Ambientes de desarrollo:**
Para levantar la aplicación, ir dentro de la carpeta del proyecto donde se encuentra el archivo **build.gradle**. ejecuta la siguiente instruccion:
```
$ ./gradlew clean bootRun
```
**Para ambientes productivos:**
En construcción...:

```
 $ pwd
```

### Dependencias

Utiliza las dependencias que se describen a continuación:

| Dependencia                  | Version               |
|------------------------------|-----------------------|
| Spring Framework             | 2.7.12                |
| Lombok                       | 1.18.26               |
| com.h2database:h2            | 2.1.214               |
| Spring-boot-starter-data-jpa | 2.7.12                |
| Spring-boot-starter-security | 2.7.12                |
| Spring-boot-starter-web      | 2.7.12 |


### ToDo

- FRONT
- SWAGGER
- MEJORAR PERFORMANCE

**Develop By:**  @LuFerMX