package com.learnig.ventast.service;

import com.learnig.ventast.exceptions.ValidaListaException;
import com.learnig.ventast.model.entity.Cliente;
import com.learnig.ventast.model.entity.ListaCompras;

import java.util.List;

public interface IListaComprasService {

    ListaCompras getClienteById(Integer idListaCompra);

    List<ListaCompras> getAllListaComprasClientes();

    Cliente agregarListaComprasCliente(final Cliente listaCompras) throws ValidaListaException;

    List<Cliente> agregarListaComprasClientesAll(final List<Cliente> clientes) throws ValidaListaException;

    Cliente actualizaListaComprasCliente(final Cliente listaCompras) throws ValidaListaException;

    String borrarListaCompras(final Integer idListaCompras);

}
