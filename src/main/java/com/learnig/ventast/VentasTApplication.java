package com.learnig.ventast;

import com.learnig.ventast.model.entity.Cliente;
import com.learnig.ventast.model.entity.ListaCompras;
import com.learnig.ventast.model.entity.ListaDetalles;
import com.learnig.ventast.model.entity.Producto;
import com.learnig.ventast.model.repository.ClienteRepository;
import com.learnig.ventast.model.repository.ListaComprasRepository;
import com.learnig.ventast.model.repository.ProductosRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.*;

@SpringBootApplication(scanBasePackages = "com.learnig.ventast")
public class VentasTApplication {

    public static void main(String[] args) {
        SpringApplication.run(VentasTApplication.class, args);
    }

    @Bean
    CommandLineRunner commandLineRunner(
            ClienteRepository clienteRepository,
            ListaComprasRepository listaComprasRepository,
            ProductosRepository productosRepository
    ){
        Calendar calendar = Calendar.getInstance();
        final Date now = calendar.getTime();
        return args -> {
            //*
            Cliente cliente1 = clienteRepository.save(new Cliente(720011, "Fernando", true, null));
            Producto producto1 = productosRepository.save(new Producto(18157, "ERGO-4580", "Esmeriladora angular 4-1/2\" 850 W, ERGO-PRO, Truper",true, null));
            //Producto producto2 = productosRepository.save(new Producto(10200, "POHE-7GCU", "Porta herramientas grande de cuero, 7 compartimentos, Truper",true, null));
            //Producto producto3 = productosRepository.save(new Producto(25035, "MAN-1/2P", "Metro de manguera 1/2\" 2 capas en rollo de 100 m, Pretul",true, null));

            //Optional<Cliente> cliente1Tmp = clienteRepository.findById(720010);
            //Optional<Producto> producto1Tmp = productosRepository.findById(18156);
            //Cliente cliente2Tmp = cliente1Tmp.get();

            ListaCompras listaCompras = new ListaCompras();
            listaCompras.setNombre("Lista Prueba");
            listaCompras.setFechaRegistro(now);
            listaCompras.setFechaUltimaActualizacion(now);
            listaCompras.setActivo(true);
            cliente1.addListaCompras(listaCompras);
            cliente1 = clienteRepository.save(cliente1);

            Objects.requireNonNull(cliente1.getListaCompras().stream().filter(listaCompras::equals).findAny().orElse(null))
                    .addListaComprasDetalles(ListaDetalles.builder()
                    .idProducto(producto1.getIdProducto())
                    .productos(producto1)
                    .cantidad(5)
                    .build());

            clienteRepository.save(cliente1);
            //System.out.println(cliente2.toString());
            // */
        };
    }

}
