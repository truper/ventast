package com.learnig.ventast.model.repository;

import com.learnig.ventast.model.entity.ListaCompras;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ListaComprasRepository extends JpaRepository<ListaCompras, Integer> {

    //@Query("SELECT lista.nombre, lista.fechaUltimaActualizacion, lista.cliente.nombre, " +
    //        "lista.cliente.idCliente, detalle.cantidad FROM ListaCompras lista JOIN FETCH lista.listaDetalles detalle")
    @Query("SELECT lista FROM ListaCompras lista")
    List<ListaCompras> retrieveAll();

}
