package com.learnig.ventast.controller;

import com.learnig.ventast.exceptions.ValidaListaException;
import com.learnig.ventast.model.entity.Cliente;
import com.learnig.ventast.model.entity.ListaCompras;
import com.learnig.ventast.service.IClientesService;
import com.learnig.ventast.service.IListaComprasService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/clientes")
public class ListaComprasController {

    private static final Log LOGGER = LogFactory.getLog(ListaComprasController.class);


    @Autowired
    private final IClientesService clientesService;

    @Autowired
    private final IListaComprasService listaComprasService;

    private static final String CLIENTE = "CLIENTE";

    @GetMapping
    public ResponseEntity<List<Cliente>> getClientes() {
        final List<Cliente> clienteList = clientesService.getAllClientes();
        return ResponseEntity.ok().body(clienteList);
    }

    @GetMapping(value = "/{idCliente}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Cliente> getCliente(@Validated @PathVariable("idCliente") final Integer idCliente) {
        final Cliente cliente = clientesService.getClienteById(idCliente);
        return ResponseEntity.ok().body(cliente);
    }

    @GetMapping(value = "/listaCompras", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ListaCompras>> getListaCompras() {
        final List<ListaCompras> listaCompras = listaComprasService.getAllListaComprasClientes();
        return new ResponseEntity<>(listaCompras, HttpStatus.OK);
    }
    @PostMapping(value = "/listaCompras", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> setListaComprasCliente(@Validated @RequestBody final Cliente cliente) throws ValidaListaException, IOException {
        LOGGER.info(CLIENTE + cliente);
        //, BindingResult binding //LOGGER.info("\n\nERROR: " + binding.getAllErrors());
        // @RequestBody InputStreamResource cliente
        /*String result = new BufferedReader(new InputStreamReader(cliente.getInputStream()))
                .lines().collect(Collectors.joining("\n"));
        LOGGER.info("CLIENTE: " + result); */
        //final Cliente listaCliente = listaComprasService.agregarListaComprasCliente(cliente);
        final Cliente listaCliente = clientesService.saveClienteLista(cliente);

        return ResponseEntity.ok(listaCliente);
    }

    @PostMapping(value = "/listaCompras/all", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> setListaComprasClientes(@Validated @RequestBody final List<Cliente> clientes) throws ValidaListaException {
        LOGGER.info(CLIENTE + clientes);
        final List<Cliente> listaClientes = clientesService.saveClientesListas(clientes);
        return ResponseEntity.ok(listaClientes);
    }

    @PutMapping(value = "/listaCompras", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> updateListaComprasCliente(@Validated @RequestBody final Cliente cliente) throws ValidaListaException {
        LOGGER.info(CLIENTE + cliente);
        final Cliente listaCliente = clientesService.updateClienteLista(cliente);
        return ResponseEntity.ok(listaCliente);
    }

    @DeleteMapping(value = "/listaCompras")
    public ResponseEntity<Object> setListaCompras(@Validated @RequestBody final Cliente cliente) throws ValidaListaException {
        String mensaje = clientesService.deleteClienteLista(cliente);
        return ResponseEntity.ok(mensaje);
    }
}
