-- Usuario
INSERT INTO usuarios (id_usuario, password) VALUES ('fer', 'SecretKey');

-- Clientes
INSERT INTO clientes (customer_id, nombre, activo) VALUES (720010, 'Fernando Barrios', true);
INSERT INTO clientes (customer_id, nombre, activo) VALUES (710020, 'Juan Carlos Bodoque', true);
INSERT INTO clientes (customer_id, nombre, activo) VALUES (710030, 'Ramiro Cuellar A', true);

-- Catalogo de productos
INSERT INTO productos (codigo_producto, clave, descripcion, activo) VALUES (18156, 'ERGO-4580', 'Esmeriladora angular 4-1/2" 850 W, ERGO-PRO, Truper',true);
INSERT INTO productos (codigo_producto, clave, descripcion, activo) VALUES (10200, 'POHE-7GCU', 'Porta herramientas grande de cuero, 7 compartimentos, Truper',true);
INSERT INTO productos (codigo_producto, clave, descripcion, activo) VALUES (25020, 'MAN-1/2P', 'Metro de manguera 1/2" 2 capas en rollo de 100 m, Pretul',true);
INSERT INTO productos (codigo_producto, clave, descripcion, activo) VALUES (10170, 'COL12-ROU-NX2', 'Collet de 1/2" para ROU-NX2, Truper',true);
INSERT INTO productos (codigo_producto, clave, descripcion, activo) VALUES (10280, 'PA-TRM-2', 'Pintura en aerosol, transparente mate,bote tradicional,400ml',true);
INSERT INTO productos (codigo_producto, clave, descripcion, activo) VALUES (30001, 'R-310N', 'Regadera satín 10" plato cuadrado delgado con brazo, Foset',true);
INSERT INTO productos (codigo_producto, clave, descripcion, activo) VALUES (15800, 'BALI-3100M', '1 banda de lija 3 x 21" p/madera grano 100 en paquete de 5',true);
