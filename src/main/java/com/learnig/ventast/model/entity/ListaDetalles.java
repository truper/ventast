package com.learnig.ventast.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "lista_detalles")
@IdClass(ListaDetalleId.class)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@ToString
public class ListaDetalles implements Serializable {

    @Id
    private Integer idListaCompra;
    @Id
    private Integer idProducto;

    private Integer cantidad;

    @ManyToOne//(fetch = FetchType.EAGER)
    @JsonBackReference(value = "lista-detalle-compras")
    private ListaCompras listaCompra;

    @ManyToOne//(fetch = FetchType.LAZY)
    @JoinColumn(name = "codigo_producto")
    @JsonBackReference
    private Producto productos;

}
