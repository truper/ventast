package com.learnig.ventast.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;


/**
 * Descripci&oacute;n: Clase Modelo que representa los atributos de la tabla
 * [<b>clientes</b>] en la base de datos [<b>truperdb</b>].
 * Se agregan los m&eacute;todos addListaCompras y removeListaCompras
 * para tener una relacioacute;n estable
 * Clase creada el 13/06/2023.
 * @author Luis Fernando Barrios Quiroz[luisf.barrios@gmail.com].
 */
@Entity
@Table(name = "clientes")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Cliente implements Serializable {

    @Id
    @Column(name = "customer_id")
    private Integer idCliente;
    @JsonIgnore
    @Column(length = 50)
    private String nombre;
    @JsonIgnore
    private Boolean activo;

    @OneToMany(
            mappedBy = "cliente",
            cascade = CascadeType.ALL,
            fetch = FetchType.EAGER
    )
    @Column(nullable = true)
    @JsonManagedReference(value = "lista-compras-cliente")
    private Set<ListaCompras> listaCompras = new HashSet<>();

    public void addListaCompras(ListaCompras listaCompra) {
        if (null == listaCompras) {
            listaCompras = new HashSet<>();
        }
        listaCompras.add(listaCompra);
        listaCompra.setCliente(this);
     }

    public void removeListaCompras(ListaCompras listaCompra) {
        listaCompras.remove(listaCompra);
        listaCompra.setCliente(null);
    }

}
