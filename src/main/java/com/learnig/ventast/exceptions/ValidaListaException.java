package com.learnig.ventast.exceptions;

public class ValidaListaException extends Exception {

    public ValidaListaException(String mensaje){
        super(mensaje);
    }
}
