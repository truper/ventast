package com.learnig.ventast.service;


import com.learnig.ventast.exceptions.ValidaListaException;
import com.learnig.ventast.model.entity.Cliente;

import java.util.List;

public interface IClientesService {

    Cliente getClienteById(Integer idCliente);

    List<Cliente> getAllClientes();

    Cliente saveCliente(final Cliente cliente);

    Cliente saveClienteLista(final Cliente clienteSave) throws ValidaListaException;

    List<Cliente> saveClientesListas(final List<Cliente> clientes) throws ValidaListaException;

    Cliente updateClienteLista(final Cliente clienteUpdate) throws ValidaListaException;

    String deleteClienteLista(final Cliente clienteDelete) throws ValidaListaException;

}
