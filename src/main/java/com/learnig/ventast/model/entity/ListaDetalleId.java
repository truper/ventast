package com.learnig.ventast.model.entity;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Descripci&oacute;n: Clase encargada de la Claves primarias compuestas en la tabla
 * [<b>lista_detalles</b>] de la base de datos [<b>truperdb</b>].
 * Se usa la anotaci&oacute;n IdClass en vez de EmbeddedId Porque es m&aacute;s simple
 * Clase creada el 14/06/2023.
 * @author Luis Fernando Barrios Quiroz[luisf.barrios@gmail.com].
 */
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class ListaDetalleId implements Serializable {
    private Integer idListaCompra;
    private Integer idProducto;

}
