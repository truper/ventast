package com.learnig.ventast.aop;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class EjecutionTiempoLog {

    private static final Log LOGGER = LogFactory.getLog(EjecutionTiempoLog.class);

    @Around("@annotation(com.learnig.ventast.aop.AnnotationTiempoLog)")
    public void AnnotationTiempoLog(ProceedingJoinPoint point) {
        try {
            Long tiempo1 = System.currentTimeMillis();
            point.proceed();
            Long tiempo2 = System.currentTimeMillis();
            long total = tiempo2 - tiempo1;
            LOGGER.info("\n\nTIEMPO TOTAL DE EJECUCIÓN(ms): " + total);
            //System.out.println("\n\nTIEMPO TOTAL DE EJECUCIÓN: " + total);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }
}
