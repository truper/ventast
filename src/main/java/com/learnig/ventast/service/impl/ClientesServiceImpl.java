package com.learnig.ventast.service.impl;

import com.learnig.ventast.exceptions.NotFountException;
import com.learnig.ventast.exceptions.ValidaListaException;
import com.learnig.ventast.model.entity.Cliente;
import com.learnig.ventast.model.entity.ListaCompras;
import com.learnig.ventast.model.entity.ListaDetalles;
import com.learnig.ventast.model.entity.Producto;
import com.learnig.ventast.model.repository.ClienteRepository;
import com.learnig.ventast.model.repository.ListaComprasRepository;
import com.learnig.ventast.service.IClientesService;
import com.learnig.ventast.service.IProductosService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class ClientesServiceImpl implements IClientesService {

    private static final Log LOGGER = LogFactory.getLog(ClientesServiceImpl.class);

    private final ClienteRepository clienteRepository;
    private final IProductosService productosService;
    private final ListaComprasRepository listaComprasRepository;


    public final String MENSAJE_ERROR_CLIENTE = "Error en los datos del cliente";
    public final String MENSAJE_ERROR_PRODUCTO = "Error en los datos del producto";
    public final Integer CERO = 0;



    public Cliente getClienteById(final Integer idCliente) {
        final Optional<Cliente> optionalCliente = clienteRepository.findById(idCliente);
        return optionalCliente.orElse(null);
    }

    public List<Cliente> getAllClientes() {
        return clienteRepository.findAll();
    }

    public Cliente saveCliente(final Cliente cliente) {
        return clienteRepository.save(cliente);
    }

    //@AnnotationTiempoLog
    public Cliente saveClienteLista(final Cliente clienteSave) throws ValidaListaException {
        Cliente cliente1 = validaCliente(clienteSave);
        if (null == cliente1) {
            throw new ValidaListaException(MENSAJE_ERROR_CLIENTE);
        }
        Calendar calendar = Calendar.getInstance();
        final Date now = calendar.getTime();

        for (ListaCompras lista: clienteSave.getListaCompras()) {
            lista.setActivo(true);
            cliente1.addListaCompras(new ListaCompras(null,
                    lista.getNombre(), now, now,
                    true,null, null));
        }
        cliente1 = clienteRepository.save(cliente1);
        for (ListaCompras lista: clienteSave.getListaCompras()) {
            cliente1.getListaCompras().stream()
                    .filter(lista::equals)
                    .forEach(listaCompras1 -> {
                        lista.getListaDetalles().forEach(detalles -> {
                            try {
                                listaCompras1.addListaComprasDetalles(
                                        new ListaDetalles(null,
                                                detalles.getIdProducto(), detalles.getCantidad(),
                                                null, validaProducto(detalles.getIdProducto()))
                                );
                            } catch (ValidaListaException e) {
                                throw new NotFountException(e);
                            }
                        });
                    });
        }
        cliente1 = clienteRepository.save(cliente1);
        return cliente1;
    }

    public List<Cliente> saveClientesListas(final List<Cliente> clientes) throws ValidaListaException {
        return clientes.stream().peek(cliente -> {
            try {
                saveClienteLista(cliente);
            } catch (ValidaListaException e) {
                throw new NotFountException(e);
            }
        }).collect(Collectors.toList());
    }

    public Cliente updateClienteLista(final Cliente clienteUpdate) throws ValidaListaException {
        Cliente cliente1 = validaCliente(clienteUpdate);
        if (null == cliente1) {
            throw new ValidaListaException(MENSAJE_ERROR_CLIENTE);
        }
        Calendar calendar = Calendar.getInstance();
        final Date now = calendar.getTime();

        for (ListaCompras lista: clienteUpdate.getListaCompras()) {
            cliente1.getListaCompras().stream()
                    .filter(listaCompras -> lista.getIdListaCompra().equals(listaCompras.getIdListaCompra()))
                    .forEach(listaCompras -> {
                       listaCompras.setNombre(lista.getNombre());
                       listaCompras.setFechaUltimaActualizacion(now);
                       lista.getListaDetalles().forEach(detalles -> {
                           try {
                               listaCompras.addListaComprasDetalles(
                                       new ListaDetalles(null,
                                               detalles.getIdProducto(), detalles.getCantidad(),
                                               null, validaProducto(detalles.getIdProducto())));
                           } catch (ValidaListaException e) {
                               throw new NotFountException(e);
                           }
                       });
                    });
        }

        cliente1 = clienteRepository.save(cliente1);
        return cliente1;
    }

    public String deleteClienteLista(final Cliente clienteDelete) throws ValidaListaException {
        Cliente cliente1 = validaCliente(clienteDelete);
        if (null == cliente1) {
            throw new ValidaListaException(MENSAJE_ERROR_CLIENTE);
        }
        for (ListaCompras lista: clienteDelete.getListaCompras()) {
            cliente1.getListaCompras().stream()
                    .filter(listaCompras -> listaCompras.getIdListaCompra().equals(lista.getIdListaCompra()))
                    .forEach(cliente1::removeListaCompras);
        }
        clienteRepository.save(cliente1);
        //listaComprasRepository.delete(listaCompras.get());
        return "Lista borrada";
    }
    private Cliente validaCliente(Cliente cliente) {
        if(null == cliente && null == cliente.getIdCliente()) {
            return null;
        }
        Optional<Cliente> clienteOptional = clienteRepository.findById(cliente.getIdCliente());
        return clienteOptional.filter(Cliente::getActivo).orElse(null);
    }

    private Producto validaProducto(final Integer idProducto) throws ValidaListaException {
        Producto producto = null;
        if(null == idProducto || idProducto.equals(CERO)) {
            throw new ValidaListaException(MENSAJE_ERROR_PRODUCTO);
        }

        try {
            producto = productosService.getProductoById(idProducto);
        } catch (IllegalArgumentException | NullPointerException e) {
            LOGGER.info("ERROR: "+ e.getMessage());
            throw new ValidaListaException(MENSAJE_ERROR_PRODUCTO);
        }
        return producto;
    }

}
