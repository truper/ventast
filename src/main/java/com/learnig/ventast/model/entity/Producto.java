package com.learnig.ventast.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Descripci&oacute;n: Clase Modelo que representa los atributos de la tabla
 * [<b>productos</b>] en la base de datos [<b>truperdb</b>].
 * Se agregan los m&eacute;todos addListaComprasDetalles y removeListaComprasDetalles
 * para tener una relacioacute;n estable
 * Clase creada el 13/06/2023.
 * @author Luis Fernando Barrios Quiroz[luisf.barrios@gmail.com].
 */
@Entity
@Table(name = "productos")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Producto {

    @Id
    @Column(name = "codigo_producto")
    private Integer idProducto;
    @Column(length = 15)
    private String clave;
    @Column(length = 150)
    private String descripcion;
    @JsonIgnore
    private Boolean activo;

    @OneToMany(mappedBy = "productos", fetch = FetchType.LAZY)
    @JsonManagedReference
    private Set<ListaDetalles> listaComprasDetalle = new HashSet<>();

    public void addListaComprasDetalles(ListaDetalles listaDetalle) {
        if (null == listaComprasDetalle) {
            listaComprasDetalle = new HashSet<>();
        }
        listaComprasDetalle.add(listaDetalle);
        Objects.requireNonNull(listaDetalle).setProductos(this);
    }

    public void removeListaComprasDetalles(ListaDetalles listaDetalle) {
        listaComprasDetalle.remove(listaDetalle);
        listaDetalle.setListaCompra(null);
    }

}
