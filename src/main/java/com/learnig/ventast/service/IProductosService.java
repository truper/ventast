package com.learnig.ventast.service;

import com.learnig.ventast.model.entity.Producto;

import java.util.List;

public interface IProductosService {

    Producto getProductoById(Integer idProducto);

    List<Producto> getAllProductos();

}
