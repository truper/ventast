package com.learnig.ventast.service.impl;

import com.learnig.ventast.aop.AnnotationTiempoLog;
import com.learnig.ventast.exceptions.ValidaListaException;
import com.learnig.ventast.model.entity.Cliente;
import com.learnig.ventast.model.entity.ListaCompras;
import com.learnig.ventast.model.entity.ListaDetalles;
import com.learnig.ventast.model.entity.Producto;
import com.learnig.ventast.model.repository.ListaComprasRepository;
import com.learnig.ventast.service.IClientesService;
import com.learnig.ventast.service.IListaComprasService;
import com.learnig.ventast.service.IProductosService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class ListaComprasServiceImpl implements IListaComprasService {

    private static final Log LOGGER = LogFactory.getLog(ListaComprasServiceImpl.class);

    private final ListaComprasRepository listaComprasRepository;

    private final IClientesService clientesService;

    private final IProductosService productosService;

    public final String MENSAJE_ERROR_CLIENTE = "Error en los datos del cliente";
    public final String MENSAJE_ERROR_PRODUCTO = "Error en los datos del producto";
    public final String MENSAJE_ERROR_LISTA = "Error en los datos";
    public final Integer CERO = 0;

    @AnnotationTiempoLog
    public ListaCompras getClienteById(final Integer idListaCompra) {
        final Optional<ListaCompras> optionalCliente = listaComprasRepository.findById(idListaCompra);
        return optionalCliente.get();
    }

    @AnnotationTiempoLog
    public List<ListaCompras> getAllListaComprasClientes() {
        return listaComprasRepository.findAll();
    }

    @AnnotationTiempoLog
    @Deprecated
    public Cliente agregarListaComprasCliente(final Cliente clienteTmp) throws ValidaListaException {
        Cliente cliente1 = validaCliente(clienteTmp);
        if (null == cliente1) {
            throw new ValidaListaException(MENSAJE_ERROR_CLIENTE);
        }
        Calendar calendar = Calendar.getInstance();
        final Date now = calendar.getTime();

        List<ListaCompras> listaCompras = clienteTmp.getListaCompras().stream().map(listaCompras1 -> {
            listaCompras1.setActivo(true);
            ListaCompras listaCompras2 = new ListaCompras();
            listaCompras2.setFechaRegistro(now);
            listaCompras2.setFechaUltimaActualizacion(now);
            listaCompras2.setActivo(true);
            listaCompras2.setNombre(listaCompras1.getNombre());
            listaCompras2.setCliente(cliente1);
            return listaCompras2;
        }).collect(Collectors.toList());
        listaCompras = listaComprasRepository.saveAll(listaCompras);

        listaCompras.forEach(listaCompras1 -> {
            clienteTmp.getListaCompras().stream()
                    .filter(listaCompras2 -> listaCompras2.equals(listaCompras1))
                    .forEach(listaCompras2 -> {
                        listaCompras1.setListaDetalles(creaDetalleCompra(listaCompras2.getListaDetalles(), listaCompras1) );
                    });
                    /*
                    Set<ListaDetalles> listaDetalles = clienteTmp.getListaCompras().stream()
                            .filter(listaCompras2 -> listaCompras2.equals(listaCompras1))
                            .map(ListaCompras::getListaDetalles).flatMap(Collection::stream)
                            .peek(detalles -> {
                                try {
                                    detalles.setProductos(validaProducto(detalles.getIdProducto()));
                                } catch (ValidaListaException e) {
                                    throw new RuntimeException(e);
                                }
                                detalles.setListaCompra(listaCompras1);
                                detalles.setIdListaCompra(listaCompras1.getIdListaCompra());
                            }).collect(Collectors.toSet());
                    listaCompras1.setListaDetalles(listaDetalles);
                     */
                });
        //listaCompras = listaComprasRepository.saveAll(listaCompras);
        cliente1.setListaCompras(new HashSet<>(listaCompras));
        clientesService.saveCliente(cliente1);
        return cliente1;
    }

    @Deprecated
    public List<Cliente> agregarListaComprasClientesAll(final List<Cliente> clientes) throws ValidaListaException {
        List<Cliente> lista = clientes.stream().peek(cliente -> {
            try {
                agregarListaComprasCliente(cliente);
            } catch (ValidaListaException e) {
                throw new RuntimeException(e);
            }
        }).collect(Collectors.toList());
        return lista;
    }

    @AnnotationTiempoLog
    @Deprecated
    public Cliente actualizaListaComprasCliente(final Cliente clienteTmp) throws ValidaListaException {
        Cliente cliente1 = validaCliente(clienteTmp);
        if (null == cliente1) {
            throw new ValidaListaException(MENSAJE_ERROR_CLIENTE);
        }
        Calendar calendar = Calendar.getInstance();
        final Date now = calendar.getTime();
        List<ListaCompras> listaComprasTmp = new ArrayList<>(clienteTmp.getListaCompras());
        //List<ListaCompras> listaCompras =
                cliente1.getListaCompras().stream()
                .forEach( listaCompras1 -> {
                    listaComprasTmp.stream()
                            .filter(listaCompras2 -> listaCompras2.getIdListaCompra().equals(listaCompras1.getIdListaCompra()))
                            .forEach(listaCompras2 -> {
                                listaCompras1.setNombre(listaCompras2.getNombre());
                                listaCompras1.setFechaUltimaActualizacion(now);
                                listaCompras1.getListaDetalles()
                                        .addAll(creaDetalleCompra(listaCompras2.getListaDetalles(),listaCompras1));
                            });
                    //return listaCompras1;
                });
                //.collect(Collectors.toList());
        //listaCompras = listaComprasRepository.saveAll(listaCompras);
        //cliente1.setListaCompras(new HashSet<>(listaCompras));
        cliente1 = clientesService.saveCliente(cliente1);
        return cliente1;
    }

    @AnnotationTiempoLog
    @Deprecated
    public String borrarListaCompras(final Integer idListaCompras) {
        if(null == idListaCompras || idListaCompras.equals(CERO) ) {
            return  MENSAJE_ERROR_LISTA;
        }
        Optional<ListaCompras> listaCompras = listaComprasRepository.findById(idListaCompras);
        listaComprasRepository.delete(listaCompras.get());
        return "Lista borrada";
    }

    private Cliente validaCliente(final Cliente cliente) {
        if(null == cliente && null == cliente.getIdCliente()) {
            return null;
        }
        final Cliente cliente1 = clientesService.getClienteById(cliente.getIdCliente());
        return cliente1.getActivo() ? cliente1 : null;
    }

    private Producto validaProducto(final Integer idProducto) throws ValidaListaException {
        Producto producto = null;
        if(null == idProducto || idProducto.equals(CERO)) {
            throw new ValidaListaException(MENSAJE_ERROR_PRODUCTO);
        }

        try {
            producto = productosService.getProductoById(idProducto);
        } catch (IllegalArgumentException | NullPointerException e) {
            LOGGER.info("ERROR: "+ e.getMessage());
            throw new ValidaListaException(MENSAJE_ERROR_PRODUCTO);
        }
        return producto;
    }

    private Set<ListaDetalles> creaDetalleCompra(Set<ListaDetalles> listaDetalles, ListaCompras listaCompras){
        return listaDetalles.stream().peek(detalles -> {
            try {
                detalles.setProductos(validaProducto(detalles.getIdProducto()));
            } catch (ValidaListaException e) {
                throw new RuntimeException(e);
            }
            detalles.setListaCompra(listaCompras);
            detalles.setIdListaCompra(listaCompras.getIdListaCompra());
        }).collect(Collectors.toSet());
    }
}
