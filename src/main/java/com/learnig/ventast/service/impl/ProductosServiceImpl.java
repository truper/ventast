package com.learnig.ventast.service.impl;

import com.learnig.ventast.model.entity.Producto;
import com.learnig.ventast.model.repository.ProductosRepository;
import com.learnig.ventast.service.IProductosService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class ProductosServiceImpl implements IProductosService {

    private final ProductosRepository productosRepository;

    public Producto getProductoById(final Integer idProducto) {
        Optional<Producto> optionalProducto = productosRepository.findById(idProducto);
        return optionalProducto.get();
    }

    public List<Producto> getAllProductos() {
        return productosRepository.findAll();
    }
}
