package com.learnig.ventast.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "lista_compras")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ListaCompras implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_lista_compra")
    private Integer idListaCompra;
    @Column(length = 50)
    private String nombre;
    @JsonIgnore
    @Column(name = "fecha_registro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaRegistro;
    @JsonIgnore
    @Column(name = "fecha_ultima_actualizacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaUltimaActualizacion;
    @JsonIgnore
    private Boolean activo;

    @ManyToOne(
            fetch = FetchType.EAGER
    )
    @JoinColumn(name = "customer_id")
    @JsonBackReference(value = "lista-compras-cliente")
    private Cliente cliente;

    @OneToMany(
            mappedBy = "listaCompra",
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY
    )
    @Column(nullable = true)
    @JsonManagedReference(value = "lista-detalle-compras")
    private Set<ListaDetalles> listaDetalles = new HashSet<>();

    public void addListaComprasDetalles(ListaDetalles listaDetalle) {
        if (null == listaDetalles) {
            listaDetalles = new HashSet<>();
        }
        listaDetalles.add(listaDetalle);
        listaDetalle.setListaCompra(this);
        listaDetalle.setIdListaCompra(this.idListaCompra);
    }

    public void removeListaComprasDetalles(ListaDetalles listaDetalle) {
        listaDetalles.remove(listaDetalle);
        listaDetalle.setListaCompra(null);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ListaCompras that = (ListaCompras) o;
        return Objects.equals(nombre, that.nombre)
                && Objects.equals(activo, that.activo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nombre, activo);
    }
}
