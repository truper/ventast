package com.learnig.ventast.model.repository;

import com.learnig.ventast.model.entity.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Descripci&oacute;n: Interface Repository genera los meacute;todos CRUD de la entidad {@link Cliente}
 * Clase creada el 13/06/2023.
 * @author Luis Fernando Barrios Quiroz[luisf.barrios@gmail.com].
 */
@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Integer> {

}
